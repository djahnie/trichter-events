---
id: '1826628697466650'
title: Mehr als wir - Jazz+Folk im D5
start: '2018-11-02 20:00'
end: '2018-11-02 22:00'
locationName: D5 - Kultur- und Bürger_innenzentrum
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/1826628697466650'
image: null
teaser: null
recurring: null
isCrawled: true
---
