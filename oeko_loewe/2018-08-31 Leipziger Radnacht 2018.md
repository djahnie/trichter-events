---
id: '889316144574891'
title: Leipziger Radnacht 2018
start: '2018-08-31 19:00'
end: '2018-08-31 22:00'
locationName: Ökolöwe - Umweltbund Leipzig e.V.
address: 'Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/889316144574891'
image: null
teaser: null
recurring: null
isCrawled: true
---
