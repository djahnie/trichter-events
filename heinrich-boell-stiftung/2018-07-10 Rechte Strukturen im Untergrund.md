---
id: rechte-strukturen-im-untergrund
title: Rechte Strukturen im Untergrund - Gespräch mit David Schraven
start: '2018-07-10 19:00'
end: '2018-07-10 21:00'
locationName: Wagenplatz am Wasserturm'
address: 
link: https://calendar.boell.de/de/event/rechte-strukturen-im-untergrund
teaser: Ein Gespräch im Rahmen der Ausstellung Weisse Wölfe, eine grafische Reportage über Neonazi-Terror
---
