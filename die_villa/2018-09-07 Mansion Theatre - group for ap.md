---
id: '1716197501803892'
title: Mansion Theatre - group for applied theatre in Leipzig
start: '2018-09-07 17:30'
end: '2018-09-07 20:30'
locationName: VILLA Leipzig
address: 'Lessingstrasse 7, 04109 Leipzig'
link: 'https://www.facebook.com/events/1716197501803892'
image: null
teaser: null
recurring: null
isCrawled: true
---
