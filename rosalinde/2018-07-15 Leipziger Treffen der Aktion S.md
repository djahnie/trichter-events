---
id: '1091064801049341'
title: Leipziger Treffen der Aktion Standesamt 2018
start: '2018-07-15 16:00'
end: '2018-07-15 18:30'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/1091064801049341'
image: null
teaser: null
recurring: null
isCrawled: true
---
