---
id: '2503402859685049'
title: Gemeinsame Anreise zur Demo "Aktion Standesamt 2018"
start: '2018-10-13 07:30'
end: '2018-10-13 15:00'
locationName: Leipzig Hauptbahnhof
address: Leipzig Hauptbahnhof
link: 'https://www.facebook.com/events/2503402859685049'
image: null
teaser: null
recurring: null
isCrawled: true
---
