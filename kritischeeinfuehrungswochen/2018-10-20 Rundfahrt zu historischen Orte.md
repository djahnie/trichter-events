---
id: '2173965202930849'
title: Rundfahrt zu historischen Orten der ArbeiterInnenbewegung
start: '2018-10-20 17:00'
end: '2018-10-20 20:00'
locationName: Treff auf dem Augustusplatz am Paulinum unter dem großen Kirchenfenster
address: Treff auf dem Augustusplatz am Paulinum unter dem großen Kirchenfenster
link: 'https://www.facebook.com/events/2173965202930849'
image: null
teaser: null
recurring: null
isCrawled: true
---
