---
id: '2225022881060835'
title: 'Ein Kennenlerntreffen für alle, die gegen Abschiebungen sind!'
start: '2018-10-25 19:00'
end: '2018-10-25 21:00'
locationName: Campus Augustusplatz / Ziegenledersaal
address: Campus Augustusplatz / Ziegenledersaal
link: 'https://www.facebook.com/events/2225022881060835'
image: null
teaser: null
recurring: null
isCrawled: true
---
