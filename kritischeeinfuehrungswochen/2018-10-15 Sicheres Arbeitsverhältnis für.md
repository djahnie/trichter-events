---
id: '247449752606866'
title: Sicheres Arbeitsverhältnis für junge Lehrer_innen?
start: '2018-10-15 15:00'
end: '2018-10-15 17:00'
locationName: Universität Leipzig
address: 'Campus Augustusplatz, Seminargebäude 420'
link: 'https://www.facebook.com/events/247449752606866'
image: null
teaser: null
recurring: null
isCrawled: true
---
