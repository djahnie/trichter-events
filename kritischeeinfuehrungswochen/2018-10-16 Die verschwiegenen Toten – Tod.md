---
id: '330306164400704'
title: Die verschwiegenen Toten – Todesopfer rechter Gewalt in Leipzig
start: '2018-10-16 17:00'
end: '2018-10-16 19:00'
locationName: Universität Leipzig
address: 'Campus Augustusplatz, S 015'
link: 'https://www.facebook.com/events/330306164400704'
image: null
teaser: null
recurring: null
isCrawled: true
---
