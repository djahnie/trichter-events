---
id: '501744243649917'
title: Barbershop Night featuring The Pizza Lab Quartet
start: '2018-11-17 18:00'
end: '2018-11-17 22:00'
locationName: Pizza LAB
address: 'Georg-Schwarz-Straße 10, 04177 Leipzig'
link: 'https://www.facebook.com/events/501744243649917'
image: null
teaser: null
recurring: null
isCrawled: true
---
