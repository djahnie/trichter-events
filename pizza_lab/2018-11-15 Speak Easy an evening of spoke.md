---
id: '2222199037804055'
title: 'Speak Easy: an evening of spoken word'
start: '2018-11-15 20:00'
end: '2018-11-15 22:00'
locationName: Pizza LAB
address: 'Georg-Schwarz-Straße 10, 04177 Leipzig'
link: 'https://www.facebook.com/events/2222199037804055'
image: null
teaser: null
recurring: null
isCrawled: true
---
