---
id: '1895831700720643'
title: 'Orga-Treffen: Planning events for the LAB!'
start: '2018-07-27 17:00'
end: '2018-07-27 18:00'
locationName: Pizza LAB
address: 'Georg-Schwarz-Straße 10, 04177 Leipzig'
link: 'https://www.facebook.com/events/1895831700720643'
image: null
teaser: null
recurring: null
isCrawled: true
---
