---
id: '2083609315286098'
title: 'Zurück zur Zukunft: Degrowth und das Recht auf Stadt'
start: '2018-11-01 19:30'
end: '2018-11-01 21:30'
locationName: Pöge-Haus
address: 'Hedwigstraße 20, 04315 Leipzig'
link: 'https://www.facebook.com/events/2083609315286098'
image: null
teaser: null
recurring: null
isCrawled: true
---
