---
id: '310929929716191'
title: 'Zukunft anders erzählen: Einsichten, Ausblicke & eine Wundertüte'
start: '2018-10-12 13:00'
end: '2018-10-12 19:00'
locationName: Refugio Berlin
address: 'Lenaustraße 3-4, 12047 Berlin'
link: 'https://www.facebook.com/events/310929929716191'
image: null
teaser: null
recurring: null
isCrawled: true
---
