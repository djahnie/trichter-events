---
id: 916-1530637200-1530640800
title: Wagenplätze gründen - offenes Treffen
start: '2018-07-03 17:00'
end: '2018-07-03 18:00'
locationName: null
address: 'Georg-Schwarz-Str. 19, Leipzig'
link: 'http://hwr-leipzig.org/event/wagenplaetze-gruenden-offenes-treffen/'
image: null
teaser: 'Ein Treffen für alle, die auf der Suche nach neuen Wagenplatzflächen in und um Leipzig sind, die Fra'
recurring: null
isCrawled: true
---
Ein Treffen für alle, die auf der Suche nach neuen Wagenplatzflächen in und um Leipzig sind, die Fragen zum Wagenleben haben. Für alle, die Menschen kennenlernen wollen, die auf Wagenplätzen wohnen möchten. 

Zweck der seit Kurzem monatlich stattfindenden Treffen ist also

ein offener Austausch und Kennenlernen. 

  

  

  

