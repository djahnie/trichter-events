---
id: 936-1543852800-1543858200
title: 'Leipziger Westen: offene Beratung für Hausprojekte'
start: '2018-12-03 16:00'
end: '2018-12-03 17:30'
locationName: null
address: 'HWR-Laden, Georg-Schwarz-Str. 19, Leipzig'
link: 'http://hwr-leipzig.org/event/leipziger-westen-offene-beratung-fuer-hausprojekte-2/'
image: null
teaser: 'Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wen'
recurring: null
isCrawled: true
---
Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wenn du Mitstreiter für deine eigene Initiative suchst; oder wenn ihr als Gruppe überlegt, welches der nächste Schritt ist, dann ist die offene Projektberatung die richtige Anlaufstelle. 

Die Veranstaltung bietet in einer Einstiegsberatung einen ersten Überblick über Herangehensweisen, Rechts- und Organisationsformen… 

Die offene Beratung beginnt am Montag, dem 3. Dezember, 16 Uhr in der Georg-Schwarz-Str. 19. Die Runde beginnt pünktlich! 

Veranstaltung des Haus- und WagenRat e.V. 

