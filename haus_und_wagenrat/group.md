---
name:  Haus- und WagenRat e.V.
website: http://hwr-leipzig.org/
email: post@hwr-leipzig.org
autocrawl:
  source: ical
  url: http://hwr-leipzig.org/veranstaltungen/?ical=1
---