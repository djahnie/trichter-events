---
id: '225055138190406'
title: Semesterauftaktparty
start: '2018-10-25 21:00'
end: '2018-10-25 04:00'
locationName: Moritzbastei
address: 'Universitätsstraße 9, 04109 Leipzig'
link: 'https://www.facebook.com/events/225055138190406'
image: null
teaser: null
recurring: null
isCrawled: true
---
