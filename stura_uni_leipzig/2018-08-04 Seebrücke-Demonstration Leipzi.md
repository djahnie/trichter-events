---
id: '419094355267638'
title: 'Seebrücke-Demonstration: Leipzig zum sicheren Hafen machen!'
start: '2018-08-04 16:00'
end: '2018-08-04 20:00'
locationName: Simsonplatz
address: 'vor dem Bundesverwaltungsgericht, Leipzig'
link: 'https://www.facebook.com/events/419094355267638'
image: null
teaser: null
recurring: null
isCrawled: true
---
