---
name:  kunZstoffe e.V.
website: http://kunzstoffe.de/

autocrawl:
    source: ical
    url: http://kunzstoffe.de/event/?ical=1
    exclude:
       - 650-1530489600-1533340799
---
