---
id: 435-1537182000-1537192800
title: Upcycle it! Weben mit Resttextilien
start: '2018-09-17 11:00'
end: '2018-09-17 14:00'
locationName: null
address: 'Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/upcycle-it-weben-mit-resttextilien/'
image: null
teaser: |-
  MIT: Marie Wagner (Kunstpäd.)
  FÜR: Alle Menschen die Lust haben einfache handwerkliche Methoden zum 
recurring: null
isCrawled: true
---
MIT: Marie Wagner (Kunstpäd.)

FÜR: Alle Menschen die Lust haben einfache handwerkliche Methoden zum Thema Upcycling zu erlernen. In gemütlicher Atmosphäre arbeiten wir mit Restmaterialien, helfen uns gegenseitig und finden gemeinsam Lösungen zu unseren handwerklichen und kreativen Prozessen. Ohne Vorkenntnisse. 

Kennst du auch das Problem, wohin mit dem ganzen Krempel, der sich teilweise über Jahre ansammelt? Wegwerfen ist für dich keine Alternative? Dann zeigen wir dir in unseren Workshops, was du alles damit anstellen kannst, getreu dem Motto „Aus alt mach Neu“! 

ANMELDUNG: verbindlich an upcycle.it@web.de – spontan dazukommen möglich, wenn Platz frei oder per Telefon 0163 4846916 (kunZstoffe e.V.) 

