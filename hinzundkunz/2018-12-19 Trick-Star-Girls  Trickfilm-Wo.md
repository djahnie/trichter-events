---
id: 1254-1545231600-1545246000
title: Trick-Star-Girls | Trickfilm-Workshop für 12 bis 16-jährige Mädchen
start: '2018-12-19 15:00'
end: '2018-12-19 19:00'
locationName: null
address: 'Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/trick-star-girls-trickfilm-workshop-fuer-12-bis-16-jaehrige-maedchen-7/'
image: null
teaser: |-
  LERNE MIT UNS SELBST TRICKFILME HERZUSTELLEN.  
  Wer:
  weiblich, 12 – 16 Jahre, offen für Neues // erf
recurring: null
isCrawled: true
---
LERNE MIT UNS SELBST TRICKFILME HERZUSTELLEN.  

Wer:

weiblich, 12 – 16 Jahre, offen für Neues // erfinderisch // Teamplayer 

Wann:

7-teilige Workshopreihe

mittwochs, 15.15 – 18.45 Uhr // 24. Oktober, 7. November, 14. November, 28. November, 5. Dezember, 12. Dezember, 19. Dezember 

Anmeldung:

buero@kunzstoffe.de, Tel. 0163/4846916

Die Teilnahme ist kostenlos. 

Alle Infos findest du HIER. 

