---
id: 1299-1542189600-1542204000
title: Humorherzen nähen
start: '2018-11-14 10:00'
end: '2018-11-14 14:00'
locationName: null
address: 'Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/humorherzen-naehen/'
image: null
teaser: |-
  WIR HELFEN MIT!!! 
  WIR ZEIGEN HERZ!!! 
  Wir wollen die Aktion „Zeige Herz“ der Leipziger Gesundheitsc
recurring: null
isCrawled: true
---
WIR HELFEN MIT!!! 

WIR ZEIGEN HERZ!!! 

Wir wollen die Aktion „Zeige Herz“ der Leipziger Gesundheitsclowns „Clowns & Clowns“ unterstützen und laden euch  herzlich ein, mit uns gemeinsam Humorherzen zu nähen, stricken und häkeln. 

Die Clowns verschenken in der Adventszeit unglaubliche 2000 Herzen bei Ihren Besuchen in Leipziger Kliniken und Pflegeheimen und zaubern so ein Lächeln auf die Lippen. Wir haben haben den Faden und ihr die Fähigkeit und freuen uns auf eine gemeinsame Runde mit Jung und Alt und gaaaaaaaanz vielen Herzen!! 

Wann?     14.11.2018, von 10:00 bis 14:00 Uhr 

                  27.11.2018, von 15:00 bis 19:00 Uhr 

Wo?          krimZkrams, Georg-Schwarz-Str.7  

