---
id: 1296-1542477600-1542484800
title: HuUpA!-Werkschau
start: '2018-11-17 18:00'
end: '2018-11-17 20:00'
locationName: null
address: 'Lindenauer Markt 13, Leipzig, Sachsen, 04177, Deutschland'
link: 'http://kunzstoffe.de/event/huupa-werkschau/'
image: null
teaser: |-
  Entdecke die Werkstücke unserer fleißigen BastlerInnen, NäherInnen, TechnikerInnen usw. 
  Mehr Infos 
recurring: null
isCrawled: true
---
Entdecke die Werkstücke unserer fleißigen BastlerInnen, NäherInnen, TechnikerInnen usw. 

Mehr Infos hier. 

