---
id: '2219916418254091'
title: 'Lektürekurs zu Karl Marx: Das Kapital. Band 2 - Erstes Treffen'
start: '2018-10-12 18:30'
end: '2018-10-12 21:30'
locationName: translib
address: 'Goetzstraße 7, 04177 Leipzig'
link: 'https://www.facebook.com/events/2219916418254091'
image: null
teaser: null
recurring: null
isCrawled: true
---
