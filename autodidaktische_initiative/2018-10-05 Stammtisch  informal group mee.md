---
id: '610055839370971'
title: Stammtisch // informal group meeting
start: '2018-10-05 20:00'
end: '2018-10-05 23:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/610055839370971'
image: null
teaser: null
recurring: null
isCrawled: true
---
