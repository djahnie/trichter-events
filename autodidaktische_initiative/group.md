---
name: autodidaktische initiative
website: http://adi-leipzig.net/
email: info@adi-leipzig.net
autocrawl:
  source: facebook_page
  id: 430986233593239
  exclude:
    - 2095710853790698 # Lesekreis „Feminismus“
    - 188268925190307 # küfa - facebook liefert seltsamerweise die falschen tage
---