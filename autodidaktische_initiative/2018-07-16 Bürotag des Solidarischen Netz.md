---
id: '280526479189135'
title: Bürotag des Solidarischen Netzwerks
start: '2018-07-16 12:00'
end: '2018-07-16 14:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/280526479189135'
image: null
teaser: null
recurring: null
isCrawled: true
---
