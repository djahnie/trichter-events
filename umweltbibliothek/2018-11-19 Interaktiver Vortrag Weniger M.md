---
id: '313658912694991'
title: 'Interaktiver Vortrag: Weniger Müll im Alltag'
start: '2018-11-19 18:00'
end: '2018-11-19 20:00'
locationName: Umweltbibliothek Leipzig / Ökolöwe
address: 'Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/313658912694991'
image: null
teaser: null
recurring: null
isCrawled: true
---
