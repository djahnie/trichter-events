---
id: '279620555944830'
title: Perspektiven auf eine zukunftsfähige Ökonomie
start: '2018-07-24 19:00'
end: '2018-07-24 21:00'
locationName: Umweltbibliothek Leipzig / Ökolöwe
address: 'Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/279620555944830'
image: null
teaser: null
recurring: null
isCrawled: true
---
