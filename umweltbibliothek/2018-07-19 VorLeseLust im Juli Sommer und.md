---
id: '209910853157289'
title: 'VorLeseLust im Juli: Sommer und Ferien'
start: '2018-07-19 16:00'
end: '2018-07-19 17:00'
locationName: Umweltbibliothek Leipzig / Ökolöwe
address: 'Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/209910853157289'
image: null
teaser: null
recurring: null
isCrawled: true
---
