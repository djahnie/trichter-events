---
name: Umweltbibliothek Leipzig
website: https://www.oekoloewe.de/umweltbibliothek-leipzig.html
email: kontakt@oekoloewe.de
autocrawl:
  source: facebook_page
  id: 583163818509582
---