---
id: 1
title: H17-Café
start: '2018-07-08 15:00'
locationName: Stadtgarten H17
address: 'Hähnelstraße 17, 04177 Leipzig'
link: 'http://freiraumsyndikat.de/?p=termine#240'
image: null
teaser: null
recurring:
  rules:
    - units: 7
      measure: days
---
