---
id: 3366-1538492400-1538503200
title: Sommerfest Bienenland
start: '2018-10-02 15:00'
end: '2018-10-02 18:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/sommerfest-bienenland/'
image: null
teaser: |-
  Externe Veranstaltung – herzlich willkommmen! 
  Willkommen im Bienenland! 
  Mitten in Leipzig entstand
recurring: null
isCrawled: true
---
Externe Veranstaltung – herzlich willkommmen! 

Willkommen im Bienenland! 

Mitten in Leipzig entstand im Jahr 2015 ein neuer Staat – das Bienenland. Aktuell residiert das Bienenland im 2. Stock des HAL Atelierhaus in der Projektwohnung Hildes Enkel: Es besitzt ein Postwesen, eine Fußballnationalmannschaft und es gibt regelmäßige Versammlungen, um die entstehenden Strukturen zu pflegen. Und wer einwandern will, der kann sich einfach einen Pass basteln. 

Einwohner:  Die BürgerInnen des Bienenlands sind aus vielen verschiedenen Ländern eingewandert, z.B. dem Iran, Sudan, Marokko, Serbien, Albanien, Deutschland oder Syrien. 

Gründungsdatum: 22.02.15 

Nationalhymne: Coco Jambo (Mr. President) 

Nationalfeiertag: Geburtstag 

Nationalgericht: Honigwaffeln 

Amtssprache: Die Landessprache heißt „biendarabisch“. Zur Stunde gibt es hier zwei allgemein anerkannte Begriffe: „Guten Bienentit!“ und „Bienenbirthday“. 

Regierungssystem: basisdemokratische Versammlung, Wahlen in unregelmäßigen Abständen 

Staatsoberhaupt: Chefs werden in den Bienenlandversammlungen von den BienenbürgerInnen bestimmt: 

aktuell kein amtierendes Staatsoberhaupt (2016) 

Chef Jungen, Chef Mädchen (November 2015) 

Chefs für Spiele, Tanz, Malen, Dekoration, Sprachen, TV und Chillen (Juli 2015) 

Währung: zur Zeit: keine. In der jüngeren Geschichte des Bienenlands gab es bereits eine Inflation und eine zeitlang wurden Geldscheine nach der Person benannt, die sie besaß. 

Das Bienenland als gelebte Utopie 

Das Bienenland ist ein offenes Angebot für Kinder aus der Gemeinschaftsunetrkunft Torgauer Straße und dem Stadtteil rund um die Eisenbahnstraße. Immer Dienstagnachmittags treffen sich die BienenbürgerInnen, um gemeinsam zu spielen, zu gestalten und die Stadt zu erkunden. In den Sommermonate vornehmlich draußen auf verschiedenen Spielplätzen und Freiräumen im Leipziger Osten. Bei schlechtem Wetter und im Winter die den Projekträumen in der Hildegardstraße 49 (Projektwohnung: Hildes Enkel). Gerne unternehmen die BienenbürgerInnen auch Ausflüge in verschiedene kulturelle Einrichtungen der Stadt – so waren wir u.a. schon bei der Kindersprechstunde des Oberbürgermeisters, im Museum der Bildenden Künste, in der Kinder- und JugendKulturWerkstatt Jojo und im Kleingarten Immergrün. 

Das Bienenland als Ort für freiwilliges Engagement, Vernetzung & Austausch 

Die Dienstags-Treffen werden von einer Gruppe engagierter Freiwilliger gestaltet und betreut. Einmal im Monat gibt es zudem ein Plenum für alle Freiwilligen, um aktuelle Entwicklungen, Erfahrungen und Probleme zu besprechen. Das Angebot umfasst nach Absprache die Abholung der Kinder in der Torgauer Straße, die gemeinsame Gestaltung des Nachmittags und das Zurückbringen der Kinder. Wichtig sind eine grundsätzliche Offenheit im Umgang mit Menschen und neuen Situationen und Geduld und Kreativität bei Übersetzungsprozessen. Du hast Lust, Teil des Bienenlands zu werden? Dann freuen wir uns über ein Nachricht an bienenland@eexistence.de 

Neben der Kinder- und Freiwilligengruppe sieht sich das Bienenland auch als Initiator für weitere, zusätzliche kulturpädagogische Projekte im Stadtteil und der Arbeit mit Geflüchteten. Aktuell führt das Bienenland in Kooperation mit Theater Vision e.V. das Projekt Stadtteilgesichter durch. 

