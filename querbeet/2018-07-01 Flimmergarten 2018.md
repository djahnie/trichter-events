---
id: 3070-1530477000-1530486000
title: Flimmergarten 2018
start: '2018-07-01 20:30'
end: '2018-07-01 23:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/flimmergarten-2018-3/'
image: null
teaser: |-
  Tag 6 des Filmfestes zur Nachhaltigkeit. Heute: 
  Wild plants
  D, CH, USA 2016, 108 min, (OmU)
  Nicolas
recurring: null
isCrawled: true
---
Tag 6 des Filmfestes zur Nachhaltigkeit. Heute: 

Wild plants

D, CH, USA 2016, 108 min, (OmU)

Nicolas Humbert 

„Wild Plants“ – das meint nicht nur Pflanzen, sondern auch Menschen. Utopisten. Querdenker. Impulsgeber. Die Menschheit steckt in einer Krise. Auswirkungen von Kapitalismus und Klimawandel lassen uns unser Verhältnis zur Natur und zum einfachen Leben neu denken. Regisseur Nicolas Humbert trifft an unterschiedlichsten Orten Menschen, die Antworten für sich gefunden haben und neue Herausforderungen und alte Traditionen miteinander verbinden. 

 

