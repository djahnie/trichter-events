---
id: 3292-1536352200-1536357600
title: Polyloid-Filmfest
start: '2018-09-07 20:30'
end: '2018-09-07 22:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/polyloid-filmfest/'
image: null
teaser: |-
  Diesen Freitag: Das Salz der Erde 
  Wim Wenders, Juliano Ribeiro Salgad / Frankreich, Brasilien 2013 
recurring: null
isCrawled: true
---
Diesen Freitag: Das Salz der Erde 

Wim Wenders, Juliano Ribeiro Salgad / Frankreich, Brasilien 2013 / 106min / OmeU 

Dokumentarisches Denkmal für den bedeutendsten sozial-dokumentarischen Fotografen der Welt: dem Brasilianer Sebastião Salgado, der fast jeden Winkel der Erde bereiste und einige der schlimmsten Katastrophen für die Menschen des 20. Jahrhunderts eindrücklich dokumentierte. Bis er sich – verzweifelt über den Zustand der Welt – mit „Genesis“ unberührten, paradiesischen Orten der Erde zuwendet. 

