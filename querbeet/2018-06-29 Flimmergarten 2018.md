---
id: 2761-1530288000-1530313200
title: Flimmergarten 2018
start: '2018-06-29 16:00'
end: '2018-06-29 23:00'
locationName: null
address: null
link: 'http://www.querbeet-leipzig.de/event/flimmergarten-2018/'
image: null
teaser: |-
  Tag 4 des Filmfestes zur Nachhaltigkeit. Heute 2 Filme: 
  16 Uhr Petterson und Findus – Kleiner Quälg
recurring: null
isCrawled: true
---
Tag 4 des Filmfestes zur Nachhaltigkeit. Heute 2 Filme: 

16 Uhr Petterson und Findus – Kleiner Quälgeist, große Freundschaft

D 2014, 87 min., FSK 0, arabische UT, mit anschließendem Gespräch mit Ali Samadi Ahadi 

Der alte Pettersson lebt allein ihn einem kleinen Haus mitten in der Natur. Trotz seiner Hühner fühlt er sich einsam. Sein Leben ändert sich schlagartig als seine Nachbarin ihm einen kleinen sprechenden Kater schenkt – Findus. Die beiden sind unzertrennlich, doch plötzlich nimmt Pettersson den ständig krähenden Hahn seines Nachbars Gustavsson auf und Findus hat Angst um seine Freundschaft mit Pettersson. Dazu gibt es Kuchen und Kaffee (für die Großen) 



 



Um 20:30 Bitteres aus Bitterfeld. Eine Bestandsaufnahme:

BRD/DDR 1988, 30 Min.

Ulrich Neumann, Rainer Hällfritzsch, Margit Miosga, (Originalfassung) 

Das war Bitteres aus Bitterfeld (Making of):

D 2005, 45 Min.

Rainer Hällfritzsch, Ulrike Hemberger, Margit Miosga, (Originalfassung) 



Zerstörte Landschaft durch Braunkohlentagebau, farbige Abgase aus dutzenden Schornsteinen, Chemieabwässer in Flüssen und Teichen – die Folgen der DDR-Chemieindustrie in Bitterfeld sind 1988 unübersehbar. Was vor Ort jeder spürt, darf jedoch kein Thema in der Öffentlichkeit sein. In den 1980er Jahren hat sich im Untergrund eine Umweltbewegung gebildet, die zusammen mit Journalisten aus der BRD unter großen Risiken für die eigene Sicherheit die Missstände in Bitterfeld dokumentieren.

Nach dem Originalfilm von 1988 zeigen wir das retrospektive Making of. Die Beteiligten aller Seiten erinnern sich fast zwanzig Jahre später an die Zustände damals und erzählen von den Schwierigkeiten, einen Dokumentarfilm unter den Bedingungen einer Diktatur zu drehen. 

 



