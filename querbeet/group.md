---
name: Initiative Querbeet
website: http://www.querbeet-leipzig.de/
email: info@querbeet-leipzig.de
autocrawl:
    source: ical
    url: http://www.querbeet-leipzig.de/events/?ical=1
---