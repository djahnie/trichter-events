---
id: 3078-1530217800-1530226800
title: Flimmergarten 2018
start: '2018-06-28 20:30'
end: '2018-06-28 23:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/flimmergarten-2018-4/'
image: null
teaser: |-
  Tag 3 des Filmfestes zur Nachhaltigkeit. Heute: 
  Do 28.6. River blue (engl.)
  C 2016, 95 min.
  David M
recurring: null
isCrawled: true
---
Tag 3 des Filmfestes zur Nachhaltigkeit. Heute: 

Do 28.6. River blue (engl.)

C 2016, 95 min.

David McIlvride, Roger Williams 

Riverblue dokumentiert die Verschmutzung der Flüsse durch die Mode- und Textilindustrie. In Zeitabständen rückt das Thema durch Meldungen von eingestürzten Fabriken meist in Südostasien in unseren Alltag bis es in der Flut von täglichen Nachrichten wieder verschwindet. Riverblue zeigt die gravierende Auswirkung durch die Produktion von Kleidung – die Vergiftung der Flüsse. Fernab vom Leben der Konsument*innen billiger Kleidung spüren die Menschen in den Herstellungsländern die unmittelbaren Folgen, nämlich die Zerstörung ihrer Lebensgrundlage. 



Einige Menschen von Foodsharing Leipzig kochen gerettete Lebensmittel. 

 



