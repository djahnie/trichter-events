---
id: 3043-1530552600-1530558000
title: Yoga im Garten
start: '2018-07-02 17:30'
end: '2018-07-02 19:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/yoga-im-garten-2/'
image: null
teaser: |-
  Diesen Montag ab 17.30 Uhr 
  Waagerecht im grünen Element versinken. Zwischen Spannung und Entspannun
recurring: null
isCrawled: true
---
Diesen Montag ab 17.30 Uhr 

Waagerecht im grünen Element versinken. Zwischen Spannung und Entspannung atmen. Sich in Stille auffordern immer wieder anzukommen. Im aufrechten Körper liegen. Grenzen dehnen.  

In 90 Minuten angeleitetem Unterricht mit Yogalehrerin Eva wirst du zu Bewegung, Atmung und Meditation eingeladen. Bitte eigene Matte mitbringen, danke! 



