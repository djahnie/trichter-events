---
id: 2891-1531162800-1531168200
title: Yoga im Garten
start: '2018-07-09 19:00'
end: '2018-07-09 20:30'
locationName: null
address: null
link: 'http://www.querbeet-leipzig.de/event/yoga-im-garten/'
image: null
teaser: |-


  Jeden Donnerstag ab 19 Uhr 
  Waagerecht im grünen Element versinken. Zwischen Spannung und Entspann
recurring: null
isCrawled: true
---




Jeden Donnerstag ab 19 Uhr 

Waagerecht im grünen Element versinken. Zwischen Spannung und Entspannung atmen. Sich in Stille auffordern immer wieder anzukommen. Im aufrechten Körper liegen. Grenzen dehnen.  

In 90 Minuten angeleitetem Unterricht wirst du zu Bewegung, Atmung und Meditation eingeladen. Bitte eigene Matte mitbringen, danke! 







