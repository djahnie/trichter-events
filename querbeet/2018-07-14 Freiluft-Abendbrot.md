---
id: 3086-1531591200-1531598400
title: Freiluft-Abendbrot
start: '2018-07-14 18:00'
end: '2018-07-14 20:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/freiluft-abendbrot-3/'
image: null
teaser: 'Mit Haut und Haar, Kraut und Stiel Gekochtes und Gerührtes möchten wir heute untereinander teilen un'
recurring: null
isCrawled: true
---
Mit Haut und Haar, Kraut und Stiel Gekochtes und Gerührtes möchten wir heute untereinander teilen und über unsere befüllten Teller hinweg über dieses und jenes plauschen. Eigener Möhren-Curry-Aufstrich, Radieschensalat oder doch was für den Grill? Gerne könnt Ihr teilen und kosten, was das Herz begehrt. Unsere Mitglieder freuen sich auf neue Gesichter! 

*Nach unserem Gartentreff um 15 Uhr setzen wir uns zum Abendbrotteilen zusammen. Gerne könnt Ihr uns schon ab dieser Zeit besuchen, denn gemeinsame Arbeiten unter freiem Himmel machen einfach mehr Spaß. 

