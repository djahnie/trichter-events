---
id: 3321-1538415000-1538420400
title: Yoga im Garten
start: '2018-10-01 17:30'
end: '2018-10-01 19:00'
locationName: null
address: null
link: 'http://www.querbeet-leipzig.de/event/yoga-im-garten-3/'
image: null
teaser: Waagerecht im grünen Element versinken. Zwischen Spannung und Entspannung atmen. Sich in Stille auff
recurring: null
isCrawled: true
---
Waagerecht im grünen Element versinken. Zwischen Spannung und Entspannung atmen. Sich in Stille auffordern immer wieder anzukommen. Im aufrechten Körper liegen. Grenzen dehnen.  

In 90 Minuten angeleitetem Unterricht wirst du zu Bewegung, Atmung und Meditation eingeladen. Bitte eigene Matte mitbringen, danke! 

