---
id: 3415-1544882400-1544900400
title: Adventsmarkt
start: '2018-12-15 14:00'
end: '2018-12-15 19:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/adventsmarkt/'
image: null
teaser: Details kommen nach…Vorfreude schönste Freude.Unser Adventsmarkt ist Teil des lebendigen Adventskale
recurring: null
isCrawled: true
---
Details kommen nach…Vorfreude schönste Freude.Unser Adventsmarkt ist Teil des lebendigen Adventskalenders der Dresdner59. Er wird unterstützt vom Kulturamt der Stadt Leipzig. Herzlichen Dank! 

Wenn Du/Sie (erneut) einen eigenen Stand betreiben möchten, dann melden Sie/Ihr sich gerne bis Ende November unter info@querbeet-leipzig.de. 

