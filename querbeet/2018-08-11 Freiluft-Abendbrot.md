---
id: 3090-1534010400-1534017600
title: Freiluft-Abendbrot
start: '2018-08-11 18:00'
end: '2018-08-11 20:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/freiluft-abendbrot-2/'
image: null
teaser: Mit Kraut und Stiel Gekochtes und Gerührtes möchten wir heute untereinander teilen und über unsere b
recurring: null
isCrawled: true
---
Mit Kraut und Stiel Gekochtes und Gerührtes möchten wir heute untereinander teilen und über unsere befüllten Teller hinweg über dieses und jenes plauschen. Eigener Möhren-Curry-Aufstrich, Radieschensalat oder doch was für den Grill? Gerne könnt Ihr teilen und kosten, was das Herz begehrt. Unsere Mitglieder freuen sich auf neue Gesichter! 

*Nach unserem Gartentreff von 15 Uhr – 18 Uhr setzen wir uns zum Abendbrotteilen zusammen. Gerne könnt Ihr uns schon ab dieser Zeit besuchen, denn gemeinsame Arbeiten unter freiem Himmel machen einfach mehr Spaß. 

